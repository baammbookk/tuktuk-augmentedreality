//
//  RouteViewController.swift
//  tuktuk-ar
//
//  Created by Rizabek on 03.09.2021.
//

import UIKit
import SceneKit
import ARKit
import CoreLocation
import MapKit

final class RouteViewController: UIViewController, Loadable, ARSCNViewDelegate {
  
  // MARK: - Properties
  
  var destinationCoordinate: CLLocationCoordinate2D?
  
  private var engine: ARKitCoreLocationEngine!
  
  private var routeFinishNode: SCNNode? = nil {
    didSet {
      oldValue?.removeFromParentNode()
      if let node = routeFinishNode {
        sceneView.scene.rootNode.addChildNode(node)
      }
    }
  }
  
  private var routeFinishView: UIView? = nil {
    didSet {
      oldValue?.removeFromSuperview()
      if let routeFinishView = routeFinishView {
        view.addSubview(routeFinishView)
      }
    }
  }
  
  private var routeDistanceLabel: UILabel? = nil {
    didSet {
      oldValue?.removeFromSuperview()
      if let label = routeDistanceLabel {
        view.addSubview(label)
      }
    }
  }
  
  private var routeFinishHint: UIView? = nil {
    didSet {
      oldValue?.removeFromSuperview()
      if let hintView = routeFinishHint {
        view.addSubview(hintView)
      }
    }
  }
  
  private lazy var sceneView: ARSCNView = {
    let scene = SCNScene()
    let sceneView = ARSCNView()
    sceneView.scene = scene
    sceneView.delegate = self
    sceneView.autoenablesDefaultLighting = true
    return sceneView
  }()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    addSubViews()
    
    engine = ARKitCoreLocationEngineImpl(
      view: sceneView,
      locationManager: NativeLocationManager.sharedInstance,
      locationEstimatesHolder: AdvancedLocationEstimatesHolder()
    )
  }
  
  override func updateViewConstraints() {
    sceneView.layout.fillSuperview()
    super.updateViewConstraints()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    let configuration = ARWorldTrackingConfiguration()
    configuration.worldAlignment = .gravityAndHeading
    sceneView.session.run(configuration)
  
    getRoute()
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    sceneView.session.pause()
  }
  
  // MARK: - Set UI
  
  private func addSubViews() {
    view.addSubview(sceneView)
    view.setNeedsUpdateConstraints()
  }
  
  // MARK: - ARSCNViewDelegate
  
  func renderer(_ renderer: SCNSceneRenderer, didRenderScene scene: SCNScene, atTime time: TimeInterval) {
    guard let routeFinishNode = routeFinishNode else { return }
    guard let parent = routeFinishNode.parent else { return }
    guard let pointOfView = renderer.pointOfView else { return }
    
    let bounds = UIScreen.main.bounds
    
    let positionInWorld = routeFinishNode.worldPosition
    let positionInPOV = parent.convertPosition(routeFinishNode.position, to: pointOfView)
    let projection = sceneView.projectPoint(positionInWorld)
    let projectionPoint = CGPoint(x: CGFloat(projection.x), y: CGFloat(projection.y))
    
    let annotationPositionInRFN = SCNVector3Make(0.0, 1.0, 0.0)
    let annotationPositionInWorld = routeFinishNode.convertPosition(annotationPositionInRFN, to: nil)
    let annotationProjection = sceneView.projectPoint(annotationPositionInWorld)
    let annotationProjectionPoint = CGPoint(x: CGFloat(annotationProjection.x), y: CGFloat(annotationProjection.y))
    let rotationAngle = Vector2.y.angle(with: (Vector2(annotationProjectionPoint) - Vector2(projectionPoint)))
    
    let screenMidToProjectionLine = CGLine(point1: bounds.mid, point2: projectionPoint)
    let intersection = screenMidToProjectionLine.intersection(withRect: bounds)
    
    let povWorldPosition: Vector3 = Vector3(pointOfView.worldPosition)
    let routeFinishWorldPosition: Vector3 = Vector3(positionInWorld)
    let distanceToFinishNode = (povWorldPosition - routeFinishWorldPosition).length
    
    DispatchQueue.main.async { [weak self] in
      guard let self = self else { return }
      guard let routeFinishHint = self.routeFinishHint else { return }
      guard let routeFinishView = self.routeFinishView else { return }
      guard let routeDistanceLabel = self.routeDistanceLabel else { return }
      let placemarkSize = RouteViewController.finishPlacemarkSize(
        forDistance: CGFloat(distanceToFinishNode),
        closeDistance: 10.0,
        farDistance: 25.0,
        closeDistanceSize: 100.0,
        farDistanceSize: 50.0
      )
      
      let distance = floor(distanceToFinishNode)
      
      let point: CGPoint = intersection ?? projectionPoint
      let isInFront = positionInPOV.z < 0
      let isProjectionInScreenBounds: Bool = intersection == nil
      
      if isInFront {
        routeFinishHint.center = point
      } else {
        if isProjectionInScreenBounds {
          routeFinishHint.center = CGPoint(
            x: reflect(point.x, of: bounds.mid.x),
            y: bounds.height
          )
        } else {
          routeFinishHint.center = CGPoint(
            x: reflect(point.x, of: bounds.mid.x),
            y: reflect(point.y, of: bounds.mid.y)
          )
        }
      }
      
      routeFinishView.center = projectionPoint
      routeFinishView.bounds.size = CGSize(width: placemarkSize, height: placemarkSize)
      routeFinishView.layer.cornerRadius = placemarkSize / 2
      
      let distanceString = "\(distance) m"
      let distanceAttrStr = RouteViewController.distanceText(forString: distanceString)
      routeDistanceLabel.attributedText = distanceAttrStr
      routeDistanceLabel.center = projectionPoint
      let size = distanceAttrStr.boundingSize(width: .greatestFiniteMagnitude)
      routeDistanceLabel.bounds.size = size
      routeDistanceLabel.transform = CGAffineTransform(rotationAngle: CGFloat(rotationAngle - .pi))
    }
  }

  // MARK: - Private funcs
  
  private func getRoute() {
    showLoading()
    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
      guard let userLocation = self.engine.userLocationEstimate()?.location.coordinate,
            let destination = self.destinationCoordinate else { return }

      let request = MKDirections.Request.init()
      request.source = MKMapItem(placemark: MKPlacemark(coordinate: userLocation))
      request.destination = MKMapItem(placemark: MKPlacemark(coordinate: destination))
      request.transportType = .walking
      let directions = MKDirections(request: request)
      directions.calculate { [weak self] (response, error) -> Void in
        guard let self = self else { return }
        self.hideLoading()
        guard let response = response else { return }
        guard let route = response.routes.first else { return }
        self.update(withRoute: route)
      }
    }
  }
  
  private func update(withRoute route: MKRoute) {
    let routePointsCount = route.polyline.pointCount
    let routePoints = route.polyline.points()
    var geoRoute: [CLLocationCoordinate2D] = []
    for pointIndex in 0..<routePointsCount {
      let point: MKMapPoint = routePoints[pointIndex]
      geoRoute.append(point.coordinate)
    }
    
    let route = geoRoute
      .map { engine.convert(coordinate: $0) }
      .compactMap { $0 }
      .map { CGPoint(position: $0) }
    
    guard route.count == geoRoute.count else {
      return
    }
    
    guard let routeFinishPoint = route.last else { return }
        
    routeFinishNode = SCNNode()
    routeFinishNode?.position = routeFinishPoint.positionIn3D
    
    routeFinishHint = makeFinishNodeHint()
    routeFinishView = makeFinishNodeView()
    routeDistanceLabel = makeDistanceLabel()
  }
  
  private func makeFinishNodeView() -> UIView {
    let nodeView = UIView()
    nodeView.backgroundColor = .systemPurple
    return nodeView
  }
  
  private func makeFinishNodeHint() -> UIView {
    let hintView = UIView()
    hintView.frame = CGRect(x: 0.0, y: 0.0, width: 50, height: 50)
    hintView.layer.cornerRadius = 25.0
    hintView.backgroundColor = .systemRed
    return hintView
  }
  
  private func makeDistanceLabel() -> UILabel {
    let label = UILabel()
    label.textColor = .white
    label.font = UIFont.systemFont(ofSize: 32.0, weight: .bold)
    label.numberOfLines = 1
    label.layer.shadowRadius = 2.0
    label.layer.shadowColor = UIColor.black.cgColor
    label.layer.shadowOpacity = 1.0
    label.layer.shadowOffset = CGSize.zero
    return label
  }
  
  private func findProjection(ofNode node: SCNNode, inSceneOfView scnView: SCNView) -> CGPoint {
    let nodeWorldPosition = node.worldPosition
    let projection = scnView.projectPoint(nodeWorldPosition)
    return CGPoint(x: CGFloat(projection.x), y: CGFloat(projection.y))
  }
  
  private func isNodeInFrontOfCamera(_ node: SCNNode, scnView: SCNView) -> Bool {
    guard let pointOfView = scnView.pointOfView else { return false }
    guard let parent = node.parent else { return false }
    let positionInPOV = parent.convertPosition(node.position, to: pointOfView)
    return positionInPOV.z < 0
  }
  
  /// RouteFinishPlacemark size driven by design requirements

  private static func finishPlacemarkSize(forDistance distance: CGFloat, closeDistance: CGFloat, farDistance: CGFloat,
                                  closeDistanceSize: CGFloat, farDistanceSize: CGFloat) -> CGFloat
  {
    guard closeDistance >= 0 else { assert(false); return 0.0 }
    guard closeDistance >= 0, farDistance >= 0, closeDistance <= farDistance else { assert(false); return 0.0 }
    
    if distance > farDistance {
      return farDistanceSize
    } else if distance < closeDistance{
      return closeDistanceSize
    } else {
      let delta = farDistanceSize - closeDistanceSize
      let percent: CGFloat = ((distance - closeDistance) / (farDistance - closeDistance))
      let size = closeDistanceSize + delta * percent
      return size
    }
  }
  
  private static func distanceText(forString string: String) -> NSAttributedString {
    return NSMutableAttributedString(string: string, attributes: [
      .strokeColor : UIColor.black,
      .foregroundColor : UIColor.white,
      .strokeWidth : -1.0,
      .font : UIFont.boldSystemFont(ofSize: 32.0)
    ])
  }
}
