//
//  PlaceCell.swift
//  tuktuk-ar
//
//  Created by Rizabek on 03.09.2021.
//

import UIKit

final class PlaceCell: UITableViewCell {
  
  // MARK: - Properties
  
  weak var viewDelegate: MainViewDelegate?
  var feature: Feature? {
    didSet {
      guard let feature = feature else { return }
      distanceLabel.text = feature.properties.distance.description + " meters away from you"
      placeNameLabel.text = feature.properties.name
      placeAddressLabel.text = feature.properties.addressLine2
    }
  }
  
  // MARK: - Views
  
  private lazy var conteinerView: UIView = {
    let view = UIView()
    view.backgroundColor = .white
    view.addStandartShadow()
    return view
  }()
  
  private lazy var placeNameLabel: UILabel = {
    let label = UILabel()
    label.textColor = .systemPurple
    label.font = .systemFont(ofSize: 17, weight: .regular)
    return label
  }()
  
  private lazy var placeAddressLabel: UILabel = {
    let label = UILabel()
    label.textColor = .lightGray
    label.numberOfLines = 2
    label.font = .systemFont(ofSize: 15, weight: .regular)
    return label
  }()
  
  private lazy var placeCategoryLabel: UILabel = {
    let label = UILabel()
    label.textColor = .systemGray
    label.font = .systemFont(ofSize: 15, weight: .regular)
    return label
  }()
  
  private lazy var distanceLabel: UILabel = {
    let label = UILabel()
    label.textColor = .systemGreen
    label.font = .systemFont(ofSize: 13, weight: .regular)
    return label
  }()
  
  private lazy var placeIcon: UIImageView = {
    let imageView = UIImageView()
    return imageView
  }()
  
  // MARK: - Init
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    selectionStyle = .none
    addSubViews()
    layout()
    addActionsToViews()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func addSubViews() {
    contentView.addSubview(conteinerView)
    [placeCategoryLabel,
     placeNameLabel,
     placeAddressLabel,
     distanceLabel, placeIcon].forEach { conteinerView.addSubview($0) }
    contentView.backgroundColor = .white
    contentView.setNeedsUpdateConstraints()
  }
  
  private func addActionsToViews() {
    let tapGets = UITapGestureRecognizer(target: self, action: #selector(didSelectPlace))
    contentView.addGestureRecognizer(tapGets)
  }
  
  // MARK: - Set Layout
  
  private func layout() {
    conteinerView.layout.pinHorizontalEdgesToSuperView(padding: 16)
    conteinerView.layout.pinVerticalEdgesToSuperView(padding: 16)
    
    distanceLabel.layout.pinTopToSuperview(constant: 12)
    distanceLabel.layout.pinLeadingToSuperview(constant: 12)
    
    placeNameLabel.layout.pinTopToView(view: distanceLabel, constant: 8)
    placeNameLabel.layout.pinLeadingToSuperview(constant: 12)
    
    placeAddressLabel.layout.pinTopToView(view: placeNameLabel, constant: 8)
    placeAddressLabel.layout.pinBottomToSuperview(constant: -12)
    placeAddressLabel.layout.pinLeadingToSuperview(constant: 12)
    placeAddressLabel.layout.pinTrailingToSuperview(constant: -12)
  }
  
  // MARK: - Actions
  
  @objc private func didSelectPlace() {
    guard let feature = self.feature else { return }
    viewDelegate?.didSelectPlace(feature)
  }
}
