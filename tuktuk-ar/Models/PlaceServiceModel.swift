//
//  PlaceServiceModel.swift
//  tuktuk-ar
//
//  Created by Rizabek on 03.09.2021.
//

import CoreLocation

struct PlaceServiceModel {
  let userCoordinate: CLLocationCoordinate2D
}
