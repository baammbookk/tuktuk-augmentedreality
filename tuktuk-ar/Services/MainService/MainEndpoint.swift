//
//  MainEndpoint.swift
//  tuktuk-ar
//
//  Created by Rizabek on 30.08.2021.
//

import Foundation

enum MainEndpoint: EndpointProtocol {
  
  case getPlaces(_ model: PlaceServiceModel)
  
  var path: String {
    
    switch self {
    case .getPlaces:
      return ""
    }
  }
  
  var method: Method {
    switch self {
    case .getPlaces:
      return .get
    }
  }
  
  var parameters: Parameters? {
    switch self {
    case .getPlaces(let model):      
      return ["categories": "commercial.supermarket",
              "filter": "circle:\(model.userCoordinate.lon),\(model.userCoordinate.lat),5000",
              "bias": "proximity:\(model.userCoordinate.lon),\(model.userCoordinate.lat)",
              "lang": "en",
              "limit": "\(ServiceConstants.limitOffset)",
              "apiKey": ServiceConstants.apiKey]
    }
  }
  
  var headers: Headers? {
    switch self {
    case .getPlaces:
      return nil
    }
  }
}

