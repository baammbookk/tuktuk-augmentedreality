//
//  MainService.swift
//  tuktuk-ar
//
//  Created by Rizabek on 30.08.2021.
//

import Foundation

protocol MainService {
  func getPlaces(_ model: PlaceServiceModel, completion: @escaping (NetworkResult<PlacesModel>) -> Void)
}

final class MainServiceImpl: MainService {
  
  let networkService: NetworkService<MainEndpoint>
  
  init(networkService: NetworkService<MainEndpoint>) {
    self.networkService = networkService
  }
  
  func getPlaces(_ model: PlaceServiceModel, completion: @escaping (NetworkResult<PlacesModel>) -> Void) {
    let endpoint = MainEndpoint.getPlaces(model)
    networkService.request(endpoint: endpoint, completion: completion)
  }
}
