//
//  AppDelegate.swift
//  tuktuk-ar
//
//  Created by Rizabek on 29.08.2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    NativeLocationManager.sharedInstance.resume()
    setupWindow()
    return true
  }
  
  func applicationDidEnterBackground(_ application: UIApplication) {
      NativeLocationManager.sharedInstance.suspend()
  }

  func applicationWillEnterForeground(_ application: UIApplication) {
      NativeLocationManager.sharedInstance.resume()
  }

  // MARK: - Setup Window
  
  private func setupWindow() {
    window = UIWindow(frame: UIScreen.main.bounds)
    let navigationController = UINavigationController()
    let assemblyBuilder = AssemblyModuleBuilder()
    let router = Router(navigationController: navigationController, assemblyBuilder: assemblyBuilder)
    router.openMainScreen()
    window?.rootViewController = navigationController
    window?.makeKeyAndVisible()
  }
}
