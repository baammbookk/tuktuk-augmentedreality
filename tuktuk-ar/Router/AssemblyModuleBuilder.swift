//
//  AssemblyModuleBuilder.swift
//  tuktuk-ar
//
//  Created by Rizabek on 30.08.2021.
//

import UIKit
import CoreLocation

protocol AssemblyBuilderProtocol {
  func createMainModule(router: RouterProtocol) -> UIViewController
  func createRouteModule(router: RouterProtocol, destinationCoordinate: CLLocationCoordinate2D) -> UIViewController
}

final class AssemblyModuleBuilder: AssemblyBuilderProtocol {
  
  // MARK: - Create Main Module
  
  func createMainModule(router: RouterProtocol) -> UIViewController {
    let view = MainViewController()
    view.title = "Places nearby"
    let networkSevice = MainServiceImpl(networkService: NetworkService<MainEndpoint>())
    let presenter = MainViewPresenter(view: view, router: router, networkService: networkSevice)
    view.presenter = presenter
    return view
  }
  
  // MARK: - Create Route Module
  
  func createRouteModule(router: RouterProtocol, destinationCoordinate: CLLocationCoordinate2D) -> UIViewController {
    let view = RouteViewController()
    view.destinationCoordinate = destinationCoordinate
    return view
  }
}
