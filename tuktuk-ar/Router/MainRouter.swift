//
//  MainRouter.swift
//  tuktuk-ar
//
//  Created by Rizabek on 30.08.2021.
//

import UIKit
import CoreLocation

protocol MainRouterProtocol {
  var navigationController: UINavigationController? { get set }
  var assemblyBuilder: AssemblyBuilderProtocol? { get set }
}

protocol RouterProtocol: MainRouterProtocol {
  func replaceRootController(on view: UIViewController)
  func popToViewController(on view: UIViewController)
  func pop(animated: Bool)
  func popToRoot()
  func dismiss()
  
  func openMainScreen()
  func openRouteScreen(_ destinationCoordinate: CLLocationCoordinate2D)
}

final class Router: RouterProtocol {
  
  // MARK: - Properties
  
  var navigationController: UINavigationController?
  var assemblyBuilder: AssemblyBuilderProtocol?
  
  private var window: UIWindow {
    guard let window =  UIApplication.shared.windows.filter({ $0.isKeyWindow }).first else {
      let window = UIWindow(frame: UIScreen.main.bounds)
      window.makeKeyAndVisible()
      return window
    }
    return window
  }
  
  init(navigationController: UINavigationController, assemblyBuilder: AssemblyBuilderProtocol) {
    self.navigationController = navigationController
    self.assemblyBuilder = assemblyBuilder
  }
  
  // MARK: - ReplaceRootController
  
  func replaceRootController(on view: UIViewController) {
    let navigationController = UINavigationController(rootViewController: view)
    window.rootViewController = navigationController
  }
  
  // MARK: - PopToRoot
  
  func popToRoot() {
    if let navigationController = navigationController {
      navigationController.popToRootViewController(animated: true)
    }
  }
  
  // MARK: - PopToViewController
  
  func popToViewController(on view: UIViewController) {
    if let navigationController = navigationController {
      navigationController.popToViewController(view, animated: true)
    }
  }
  
  // MARK: - Pop
  
  func pop(animated: Bool) {
    if let navigationController = navigationController {
      navigationController.popViewController(animated: animated)
    }
  }
  
  // MARK: - Dismiss
  
  func dismiss() {
    if let navigationController = navigationController {
      navigationController.dismiss(animated: true, completion: nil)
    }
  }
  
  // MARK: - Open Main Screen
  
  func openMainScreen() {
    if let navigationController = self.navigationController {
        guard let view = assemblyBuilder?.createMainModule(router: self) else { return }
        navigationController.viewControllers = [view]
    }
  }
  
  // MARK: - Open Route Screen
  
  func openRouteScreen(_ destinationCoordinate: CLLocationCoordinate2D) {
    if let navigationController = self.navigationController {
        guard let view = assemblyBuilder?.createRouteModule(router: self, destinationCoordinate: destinationCoordinate) else { return }
        navigationController.pushViewController(view, animated: true)
    }
  }
}
