//
//  AdvancedLocationEstimatesHolder.swift
//  tuktuk-ar
//
//  Created by Rizabek on 03.09.2021.
//

import Foundation

class AdvancedLocationEstimatesHolder: BasicLocationEstimatesHolder {
  
  override func add(_ locationEstimate: SceneLocationEstimate) {
    for estimate in estimates {
      guard !estimate.canReplace(locationEstimate) else { return }
    }
    super.add(locationEstimate)
    filter { !locationEstimate.canReplace($0) }
  }
}
