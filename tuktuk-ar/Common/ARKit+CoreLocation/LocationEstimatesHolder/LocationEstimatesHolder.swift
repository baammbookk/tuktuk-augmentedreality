//
//  LocationEstimatesHolder.swift
//  tuktuk-ar
//
//  Created by Rizabek on 03.09.2021.
//

import Foundation
import CoreLocation

protocol LocationEstimatesHolder {
  var bestLocationEstimate: SceneLocationEstimate? { get }
  var estimates: [SceneLocationEstimate] { get }
  
  func add(_ locationEstimate: SceneLocationEstimate)
  func filter(_ isIncluded: (SceneLocationEstimate) -> Bool)
}
