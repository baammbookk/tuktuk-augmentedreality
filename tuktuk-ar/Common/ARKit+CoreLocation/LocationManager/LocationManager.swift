//
//  LocationManager.swift
//  tuktuk-ar
//
//  Created by Rizabek on 03.09.2021.
//

import Foundation
import CoreLocation

protocol LocationManager {
  var location: CLLocation? { get }
  var configuration: LocationManagerConfiguration { get set }
  
  func addListener(_ listener: LocationManagerListener)
  func removeListener(_ listener: LocationManagerListener)
  
  func suspend()
  func resume()
}

enum DesiredAccuracy {
  case bestForNavigation
  case best
  case distance(Double)
}

struct LocationManagerConfiguration {
  var desiredAccuracy: DesiredAccuracy
  var distanceFilter: Double
  var updateFrequencyFilter: TimeInterval?
  var allowsUseInBackground: Bool
  
  init(desiredAccuracy: DesiredAccuracy, distanceFilter: Double,
       updateFrequencyFilter: TimeInterval? = nil, allowsUseInBackground: Bool = false)
  {
    self.desiredAccuracy = desiredAccuracy
    self.distanceFilter = distanceFilter
    self.updateFrequencyFilter = updateFrequencyFilter
    self.allowsUseInBackground = allowsUseInBackground
  }
}

protocol LocationManagerListener: AnyObject {
  func onLocationUpdate(_ location: CLLocation)
  func onAuthorizationStatusUpdate(_ authorizationStatus: CLAuthorizationStatus)
}
