//
//  Loadable.swift
//  tuktuk-ar
//
//  Created by Rizabek on 30.08.2021.
//

import UIKit

protocol Loadable {
  func showLoading()
  func hideLoading()
}

private let loadingViewTag = 10

extension Loadable {

  func showLoading() {
    guard let self = self as? UIViewController else { return }
    let loadingView = UIView()
    loadingView.tag = loadingViewTag
    loadingView.backgroundColor = .white
    loadingView.addStandartShadow()
    self.view.addSubview(loadingView)
    
    loadingView.layout.centerHorizontally()
    loadingView.layout.centerVertically()
    loadingView.layout.size(36)
    
    let loadingIndicator = UIActivityIndicatorView(style: .medium)
    loadingView.addSubview(loadingIndicator)
    loadingIndicator.layout.centerHorizontally()
    loadingIndicator.layout.centerVertically()
    loadingIndicator.layout.size(16)
    loadingIndicator.startAnimating()
    self.view.isUserInteractionEnabled = false
  }
  
  func hideLoading() {
    guard let self = self as? UIViewController else { return }
    guard let loadingView = self.view.viewWithTag(loadingViewTag) else { return }
    loadingView.removeFromSuperview()
    self.view.isUserInteractionEnabled = true
  }
}
