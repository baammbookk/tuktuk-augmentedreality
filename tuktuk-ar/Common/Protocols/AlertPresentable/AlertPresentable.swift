//
//  AlertPresentable.swift
//  tuktuk-ar
//
//  Created by Rizabek on 30.08.2021.
//

import UIKit

protocol AlertPresentable: AnyObject {
  func showAlert(title: String?, message: String?, actions: [AlertAction], style: UIAlertController.Style)
  func alert(message: String, title: String)
}

extension AlertPresentable where Self: UIViewController {
  func alert(message: String, title: String = "") {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
    alertController.addAction(OKAction)
    self.present(alertController, animated: true, completion: nil)
  }
  
  func showAlert(title: String?, message: String?, actions: [AlertAction], style: UIAlertController.Style) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: style)
    let alertActions = actions.map { (action) in
      return UIAlertAction(title: action.title, style: action.style, handler: { (_) in
        action.action?()
      })
    }
    alertActions.forEach {
      alert.addAction($0)
    }
    present(alert, animated: true, completion: nil)
  }
}

let AlertCancelAction = AlertAction(title: "Cancel", style: .cancel, action: nil)
let AlertOkAction = AlertAction(title: "OK", style: .cancel, action: nil)
