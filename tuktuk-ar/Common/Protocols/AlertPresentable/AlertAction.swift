//
//  AlertAction.swift
//  tuktuk-ar
//
//  Created by Rizabek on 30.08.2021.
//

import UIKit

struct AlertAction {
  let title: String
  let style: UIAlertAction.Style
  let action: (() -> Void)?
}
