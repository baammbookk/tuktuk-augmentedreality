//
//  NSAttributedStringExtension.swift
//  tuktuk-ar
//
//  Created by Rizabek on 03.09.2021.
//

import UIKit

extension NSAttributedString {
  
  func boundingRect(size: CGSize) -> CGRect {
    let rect = self.boundingRect(with: size, options: .usesLineFragmentOrigin, context: nil)
    return rect.integral
  }
  
  func boundingRect(width: CGFloat) -> CGRect {
    return boundingRect(size: CGSize(width: width, height: .infinity))
  }
  
  func boundingSize(width: CGFloat) -> CGSize {
    return boundingRect(width: width).size
  }
}
