//
//  UITableView + ReloadAnimation.swift
//  tuktuk-ar
//
//  Created by Rizabek on 03.09.2021.
//


import UIKit

extension UITableView {
  func animationReload() {
    self.reloadData()
    self.layoutIfNeeded()
    let tableHeight: CGFloat = self.bounds.size.height
    let cells = self.visibleCells.prefix(10)
    var index = 0
    cells.forEach {
      let cell: UITableViewCell = $0 as UITableViewCell
      cell.transform = CGAffineTransform(translationX: -tableHeight, y: 0)
      UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseIn, animations: {
          cell.transform = CGAffineTransform(translationX: 0, y: 0);
      }, completion: nil)
      index += 1
    }
  }
}
